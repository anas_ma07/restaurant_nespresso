package com.nespresso.sofa.recruitment.restaurant;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.parser.RestaurantParser;


public class Restaurant {
	
	private Kitchen kitchen ;

	public Restaurant(String... stock) {
		kitchen = new Kitchen(stock);
	}

	public Ticket order(String order_) {
		Map<Integer, Dish> order = RestaurantParser.parseOrder(order_);
		Map.Entry<Integer, Dish> orderentry = order.entrySet().iterator().next();
		
		return kitchen.prepare(orderentry.getKey(),orderentry.getValue());
	}
	
	public Meal retrieve(Ticket ticket) {
		return kitchen.giveMeal(ticket);
	}


}
