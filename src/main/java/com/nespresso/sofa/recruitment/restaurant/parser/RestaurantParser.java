package com.nespresso.sofa.recruitment.restaurant.parser;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.Dish;
import com.nespresso.sofa.recruitment.restaurant.Ingredients;

public class RestaurantParser {

	public static Map<Ingredients, Integer> parseIngredient(String element) {
		Map<Ingredients, Integer> ingredient = new HashMap<Ingredients, Integer>();
		
		String elementWhithoutQuantity_ = element.replaceAll("\\d+", "").trim();
		Ingredients elementWhithoutQuantity = Ingredients.to(elementWhithoutQuantity_.trim());

		int quantity = Integer.MAX_VALUE;
		if(!element.replaceAll("\\D+","").trim().equals(""))
			quantity = Integer.valueOf(element.replaceAll("\\D+","").trim());

		ingredient.put(elementWhithoutQuantity, quantity);
		return ingredient;
	}
	
	public static Map<Integer, Dish> parseOrder(String order_) {
		Map<Integer, Dish> order = new HashMap<Integer, Dish>();
		int quantity = Integer.valueOf(order_.replaceAll("\\D+",""));
		String dish_ = order_.replaceAll("\\d+", "").trim();
		Dish dish = Dish.to(dish_);
		
		order.put(quantity, dish);
		return order;
	}
	
	
}
