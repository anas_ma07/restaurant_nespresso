package com.nespresso.sofa.recruitment.restaurant;


public enum Dish {

	TOMATO_MOZZARELLA_SALAD("Tomato Mozzarella Salad",6,Ingredients.MOZZARELLA,Ingredients.TOMATOES,Ingredients.TOMATOES,Ingredients.OLIVE_OIL);
	
	
	private final String value;
	public final int preparationTime;
	public final Ingredients[] ingredients;

	Dish(String value, int preparationTime,Ingredients... ingredients){
		this.value = value;
		this.preparationTime = preparationTime;
		this.ingredients = ingredients;
	}

	public static Dish to(String value){
		for (Dish dish : values()) {
			if(dish.value.equals(value))
				return dish;
		}
		return null;
	}

	@Override
	public String toString() {
		return this.value;
	}

}
