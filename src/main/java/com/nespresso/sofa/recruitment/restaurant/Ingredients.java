package com.nespresso.sofa.recruitment.restaurant;

public enum Ingredients {

	 MOZZARELLA("balls Mozzarella"),
	 TOMATOES("tomatoes"),
	 OLIVE_OIL("olive oil"),
	 PEPPER("pepper"),
	 WATER("water"),
	 FLOUR("Flour");
	 
	 private String value;
	 
	 Ingredients(String value) {
		 this.value = value;
	 }
	 
	 public static Ingredients to(String value){
		for (Ingredients element : values()) {
			if(element.value.equals(value))
				return element;
		}
		return null;
	}
	 
	 @Override
	 public String toString() {
		 return this.value;
	 }
	 
	 
}
