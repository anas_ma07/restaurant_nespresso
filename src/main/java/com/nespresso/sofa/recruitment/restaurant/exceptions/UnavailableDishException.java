package com.nespresso.sofa.recruitment.restaurant.exceptions;

public class UnavailableDishException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnavailableDishException() {
		super();
	}

	public UnavailableDishException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UnavailableDishException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnavailableDishException(String message) {
		super(message);
	}

	public UnavailableDishException(Throwable cause) {
		super(cause);
	}

}
