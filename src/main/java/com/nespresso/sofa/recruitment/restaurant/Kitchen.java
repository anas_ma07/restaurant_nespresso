package com.nespresso.sofa.recruitment.restaurant;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.nespresso.sofa.recruitment.restaurant.exceptions.UnavailableDishException;
import com.nespresso.sofa.recruitment.restaurant.parser.RestaurantParser;

public class Kitchen {

	private Map<Ingredients, Integer> stock ;
	private Map<Ticket,Meal> meals; 
	
	
	public Kitchen(String[] stock) {
		this.stock = new HashMap<Ingredients, Integer>();
		for (String element : stock) {
			this.stock.putAll(RestaurantParser.parseIngredient(element));
		}
	}
	
	public Ticket prepare(int quantity, Dish dish){
		
		Ingredients[] ingredientsOfDish = dish.ingredients;
		destock(quantity,ingredientsOfDish);
		this.meals = new HashMap<Ticket, Meal>();
		Meal meal = new Meal(quantity, dish);
		Ticket ticket = new Ticket();
		this.meals.put(ticket, meal);
		
		return ticket;
	}
	
	public Meal giveMeal(Ticket ticket) {
		return this.meals.get(ticket);
	}
	
	private void destock(int quantity, Ingredients[] ingredients) {
		Map<Ingredients, Integer> ingredientsQuantity = initalizeMap(ingredients);
		Ingredients element;
		for (Entry<Ingredients, Integer> elementQuantity : ingredientsQuantity.entrySet()) {
			element = elementQuantity.getKey();
			if(quantity*elementQuantity.getValue() > stock.get(element))
				throw new UnavailableDishException();
			stock.put(element, stock.get(element)-1);
		}
	}
	
	private Map<Ingredients, Integer> initalizeMap(Ingredients[] ingredients) {
		Map<Ingredients, Integer> ingredientsQuantity = new HashMap<Ingredients, Integer>();
		//initial
		for (Ingredients element : ingredients) {
			ingredientsQuantity.put(element, 0);
		}
		//constuct
		for (Ingredients element : ingredients) {
			ingredientsQuantity.put(element, ingredientsQuantity.get(element)+1);
		}
		return ingredientsQuantity;
	}

}
