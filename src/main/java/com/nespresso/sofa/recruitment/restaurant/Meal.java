package com.nespresso.sofa.recruitment.restaurant;

import java.util.HashMap;
import java.util.Map;

public class Meal {

	private Map<Dish, Integer> dishs;
	
	
	public Meal(Integer quantity ,Dish dish) {
		this.dishs = new HashMap<Dish, Integer>();
		this.dishs.put(dish, quantity);
	}

	public int servedDishes() {
		return dishs.entrySet().iterator().next().getValue();
	}

	public String cookingDuration() {
		Map.Entry<Dish, Integer> dish = dishs.entrySet().iterator().next();
		
		int quantity = dish.getValue();
		int duration = dish.getKey().preparationTime;
		
		Integer cookingDuration = duration + (duration/2)*(quantity-1);
		return cookingDuration.toString();
	}

}
